﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using YukiFrameWork.Extension;

namespace YukiFrameWork
{
#if UNITY_EDITOR
    public class LocalConfigWindow : EditorWindow
    {
        [InitializeOnLoadMethod]
        public static void Init()
        {
            var info = Resources.Load<LocalConfigInfo>("LocalConfigInfo");

            if (info == null)
            {
                info = ScriptableObject.CreateInstance<LocalConfigInfo>();

                string infoPath = "Assets/Resources";
                if (!Directory.Exists(infoPath))
                {
                    Directory.CreateDirectory(infoPath);
                    AssetDatabase.Refresh();
                }

                AssetDatabase.CreateAsset(info, infoPath + "/LocalConfigInfo.asset");
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(info);
                AssetDatabase.SaveAssets();
            }
        }     
    }
#endif  
}
