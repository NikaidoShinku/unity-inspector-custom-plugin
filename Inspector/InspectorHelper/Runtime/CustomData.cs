﻿///=====================================================
/// - FileName:      CustomData.cs
/// - NameSpace:     YukiFrameWork
/// - Description:   通过本地的代码生成器创建的脚本
/// - Creation Time: 2024/4/7 18:25:05
/// -  (C) Copyright 2008 - 2024
/// -  All Rights Reserved.
///=====================================================
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace YukiFrameWork
{
    public class AssemblyHelper
    {
        private static Dictionary<string, Type> typeDict = new Dictionary<string, Type>();

        public static Type GetType(string typeName, Assembly assembly = default)
        {
            if (typeDict.TryGetValue(typeName, out var type))
            {
                return type;
            }

            // 处理泛型类型          
            if (typeName.Contains('`'))
            {
                int backtickIndex = typeName.IndexOf('`');
                int genericArgStartIndex = typeName.IndexOf("[", StringComparison.Ordinal);
                if (genericArgStartIndex > backtickIndex)
                {
                    string genericTypeName = typeName.Substring(0, genericArgStartIndex);

                    string args = typeName.Substring(genericArgStartIndex).Trim('[', ']');

                    var argumentTypes = args.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                             .Select(t => GetType(t, assembly))
                                             .ToArray();
                    if (argumentTypes.Any(t => t == null)) return null;
                    else
                    {
                        if (assembly != default)
                            type = assembly.GetType(genericTypeName)?.MakeGenericType(argumentTypes);
                        type ??= Type.GetType(genericTypeName)?.MakeGenericType(argumentTypes);
                    }
                    if (type != null)
                    {

                        typeDict.Add(typeName, type);
                        return type;
                    }

                }
            }

            // 处理数组类型           
            if (typeName.EndsWith("[]") || typeName.Contains("[,"))
            {
                var arrayTypeSpecifierIndex = typeName.IndexOf('[');
                string elementTypeString = typeName.Substring(0, arrayTypeSpecifierIndex);
                string arraySpecifier = typeName.Substring(arrayTypeSpecifierIndex);

                Type elementType = GetType(elementTypeString);
                if (elementType == null) return null;

                if (arraySpecifier.Contains(","))
                {
                    int rank = arraySpecifier.Count(c => c == ',') + 1;
                    type = Array.CreateInstance(elementType, new int[rank]).GetType();
                }
                else
                {
                    type = Array.CreateInstance(elementType, 0).GetType();
                }

                if (type != null)
                {
                    typeDict.Add(typeName, type);
                    return type;
                }
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            for (int j = 0; j < assemblies.Length; j++)
            {
                Type[] types = assemblies[j].GetTypes();
                for (int i = 0; i < types.Length; i++)
                {
                    if (types[i].FullName == typeName)
                    {
                        typeDict.Add(typeName, types[i]);
                        return types[i];
                    }
                }
            }

            return null;
        }

        public static Type[] GetTypes(Type type)
        {
            return Assembly.GetAssembly(type).GetTypes();
        }

        public static Type[] GetTypes(Assembly assembly)
        {
            return assembly?.GetTypes();
        }

    }
    [System.Serializable]
    public class CustomData : GenericDataBase
    {
        [SerializeField]
        private string completedFile;
        [SerializeField]
        private bool isAutoMation = true;
        [SerializeField]
        private int autoArchitectureIndex;
        [SerializeField]
        private List<string> autoInfos = new List<string>();
        [SerializeField]
        private string assetPath = "Assets";
        [SerializeField]
        private bool isCustomAssembly = false;

        public string AssetPath
        {
            get { return assetPath; }
            set { assetPath = value; }
        }

        public string CompletedFile
        {
            get => completedFile;
            set => completedFile = value;
        }
        public bool IsAutoMation
        {
            get => isAutoMation;
            set => isAutoMation = value;
        }

        public int AutoArchitectureIndex
        {
            get => autoArchitectureIndex;
            set => autoArchitectureIndex = value;
        }

        public bool IsCustomAssembly
        {
            get => isCustomAssembly;
            set => isCustomAssembly = value;
        }

        public List<string> AutoInfos => autoInfos;

        private bool isPartialLoading = false;

        public bool IsPartialLoading
        {
            get => isPartialLoading;
            set => isPartialLoading = value;
        }

    }

}
