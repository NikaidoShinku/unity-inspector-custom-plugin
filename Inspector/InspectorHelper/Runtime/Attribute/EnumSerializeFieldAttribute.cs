﻿///=====================================================
/// - FileName:      EnumSerializableAttribute.cs
/// - NameSpace:     YukiFrameWork
/// - Description:   通过本地的代码生成器创建的脚本
/// - Creation Time: 2024/4/2 17:58:58
/// -  (C) Copyright 2008 - 2024
/// -  All Rights Reserved.
///=====================================================
using YukiFrameWork;
using UnityEngine;
using System;
namespace YukiFrameWork
{
	/// <summary>
	/// 在Label特性于枚举中不可用时，给对应的字段标记该特性
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property,AllowMultiple = false,Inherited = false)]
	public sealed class EnumSerializeFieldAttribute : PropertyAttribute
	{

	}
}
