﻿///=====================================================
/// - FileName:      GUIGroupAttribute.cs
/// - NameSpace:     YukiFrameWork
/// - Description:   通过本地的代码生成器创建的脚本
/// - Creation Time: 2024/3/3 17:33:24
/// -  (C) Copyright 2008 - 2024
/// -  All Rights Reserved.
///=====================================================
using YukiFrameWork;
using UnityEngine;
using System;
namespace YukiFrameWork
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public sealed class GUIGroupAttribute : PropertyAttribute
	{
		public string GroupName { get; private set; }
		public float Height { get; private set; } = 5;
		public GUIGroupAttribute(string groupName)
		{
			this.GroupName = groupName;			
		}

		/// <summary>
		/// 设置组别的分割高度，只需要给第一个该名称的组设置高度即可
		/// </summary>
		/// <param name="groupName"></param>
		/// <param name="height"></param>
        public GUIGroupAttribute(string groupName, float height)
        {
            this.GroupName = groupName;
            this.Height = height;
        }
    }
}
